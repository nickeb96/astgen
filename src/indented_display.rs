
pub const BASE_INDENT_STR: &'static str = "    ";


pub trait IndentedDisplay {
    fn indented_fmt(&self, indentation: u32, f: &mut std::fmt::Formatter) -> std::fmt::Result;
}


pub fn indent_text(text: &str, indentation: usize) -> String {
    let mut ret = String::new();
    let indentation = BASE_INDENT_STR.repeat(indentation);
    for line in text.lines() {
        ret.push_str(&indentation);
        ret.push_str(line);
        ret.push('\n');
    }
    ret
}
