
use std::iter::Peekable;
use crate::ctype::CType;
use crate::ctype::cinteger::{CInteger, Signedness};
use crate::ctype::cstruct::CStruct;
use crate::ctype::cfunction::CFunction;
use crate::expression::Expression;
use crate::statement::Statement;
use crate::statement::block_statement::BlockStatement;
use crate::identifier::Identifier;
use crate::parsable::Parsable;


#[derive(Debug)]
pub enum Definition {
    Function(Identifier, CFunction, BlockStatement),
    Struct(CStruct),
    GlobalVariable(Identifier, CType, Option<Expression>),
}

impl std::fmt::Display for Definition {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Definition::Function(identifier, cfunction, block_statement) => {
                cfunction.display_with_identifier(identifier, f)?;
                writeln!(f, " {}", block_statement)
            }
            Definition::Struct(cstruct) => {
                writeln!(f, "{}", cstruct)
            }
            _ => unimplemented!()
        }
    }
}
