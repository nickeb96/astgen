
use std::iter::Peekable;

use crate::expression::Expression;
use crate::statement::Statement;
use crate::ctype::CType;
use crate::indented_display::indent_text;
use crate::parsable::{Parsable, ParseError};


#[derive(Debug, Clone, Eq, PartialEq)]
pub struct BlockStatement {
    statements: Vec<Statement>,
}

impl BlockStatement {
    pub fn new(statements: Vec<Statement>) -> Self {
        Self { statements }
    }

    pub fn iter_statements(&self) -> impl Iterator<Item=&Statement> {
        self.statements.iter()
    }
}

impl Parsable for BlockStatement {
    fn parse<'a, I: Iterator<Item=&'a str>>(iter: &mut Peekable<I>) -> Result<Self, ParseError> {
        let opening_brace = iter.next();
        assert_eq!(opening_brace, Some("{"));
        let mut statements: Vec<Statement> = Vec::new();

        loop {
            match iter.peek() {
                None => return Err(ParseError::new("unclosed brace")),
                Some(&"}") => {
                    iter.next();
                    break;
                }
                _ => {
                    statements.push(crate::statement::parser::parse_statement(iter));
                }
            }
        }

        Ok(BlockStatement::new(statements))
    }
}

impl std::fmt::Display for BlockStatement {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut string = String::new();
        for statement in self.statements.iter() {
            string.push_str(&format!("{}", statement));
        }
        string = indent_text(&string, 1);
        write!(f, "{{\n{}}}", string)
    }
}

