
use crate::expression::Expression;
use crate::ctype::CType;
use crate::statement::block_statement::BlockStatement;
use crate::identifier::Identifier;
use crate::indented_display::indent_text;


#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Statement {
    Single(Expression),
    Block(BlockStatement),
    VariableDeclaration(Identifier, CType, Option<Expression>),
    IfElse(Expression, Box<Statement>, Option<Box<Statement>>),
    WhileLoop(Expression, Box<Statement>),
    /*
    ForLoop(Option<Box<Statement>>, Option<Expression>, Option<Expression>, Box<Statement>),
    DoWhileLoop(Expression, Box<Statement>),
    */
    Return(Option<Expression>),
    Break,
    Continue,
}


impl std::fmt::Display for Statement {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use Statement::*;
        match self {
            Single(expression) => writeln!(f, "{};", expression),
            Block(block) => writeln!(f, "{}", block),
            VariableDeclaration(identifier, ctype, init) => {
                ctype.display_with_identifier(identifier, f)?;
                match init {
                    Some(expression) => writeln!(f, " = {};", expression),
                    None => writeln!(f, ";"),
                }
            }
            IfElse(condition, true_case, false_case) => {
                write!(f, "if ({})", condition)?;
                match true_case {
                    box Block(block) if false_case.is_some() => write!(f, " {} ", block)?,
                    box Block(block) => write!(f, " {}\n", block)?,
                    other => write!(f, "\n{}\n", indent_text(&other.to_string(), 1))?
                }
                match false_case {
                    Some(box Block(block)) => write!(f, "else {}\n", block)?,
                    Some(other) => write!(f, "else\n{}\n", indent_text(&other.to_string(), 1))?,
                    None => (),
                }
                Ok(())
            }
            WhileLoop(condition, body) => {
                write!(f, "while ({})", condition)?;
                match body {
                    box Block(block) => write!(f, " {}\n", block),
                    other => write!(f, "\n{}\n", indent_text(&other.to_string(), 1)),
                }
            }
            Return(Some(expression)) => writeln!(f, "return {};", expression),
            Return(None) => writeln!(f, "return;"),
            _ => unimplemented!(),
        }
    }
}
