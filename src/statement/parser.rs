
/// Change the iter arg to a peekable struct so you can see the next item
/// without having to modify it which would be enough to call a delegate
/// function.

use std::iter::Peekable;
use crate::ctype::{self, CType};
use crate::expression::Expression;
use crate::expression::parser::{Fragment, fragment_list_into_expression};
use crate::statement::Statement;
use crate::statement::block_statement::BlockStatement;
use crate::parsable::{Parsable, ParseError};


pub fn parse_statement<'a, I: Iterator<Item=&'a str>>(iter: &mut Peekable<I>) -> Statement {
    if let Some(&next_token) = iter.peek() {
        match next_token {
            "if" => parse_if_statement(iter),
            "while" => parse_while_statement(iter),
            "{" => Statement::Block(BlockStatement::parse(iter).unwrap()),
            "return" => parse_return_statement(iter),
            token if crate::ctype::is_type(token) => parse_var_decl_statement(iter),
            token => parse_single_statement(iter),
        }
    } else {
        unreachable!()
    }
}


// TODO: return Result<Vec<_>, ParseError> instead
pub fn parse_parenthesis<'a, I: Iterator<Item=&'a str>>(iter: &mut I) -> Vec<&'a str> {
    let opening_parenthesis = iter.next();
    assert_eq!(opening_parenthesis, Some("("));
    let mut depth = 1usize;
    let mut sublist = Vec::new();

    loop {
        match iter.next() {
            Some("(") => {
                depth += 1;
                sublist.push("(");
            }
            Some(")") if depth == 1 => {
                return sublist;
            }
            Some(")") => {
                depth -= 1;
                sublist.push(")");
            }
            Some(other) => sublist.push(other),
            None => panic!("unmatched parenthesis"),
        }
    }
}


fn parse_single_statement<'a, I: Iterator<Item=&'a str>>(iter: &mut I) -> Statement {
    let expression_tokens = iter.take_while(|&token| token != ";");
    let expression = fragment_list_into_expression(expression_tokens);

    Statement::Single(expression)
}


/*
fn parse_block_statement<'a, I: Iterator<Item=&'a str>>(iter: &mut Peekable<I>) -> Statement {
    let opening_brace = iter.next();
    assert_eq!(opening_brace, Some("{"));

    let mut statements: Vec<Statement> = Vec::new();

    loop {
        match iter.peek() {
            None => panic!("unclosed brace"),
            Some(&"}") => {
                iter.next();
                break;
            }
            _ => {
                statements.push(parse_statement(iter));
            }
        }
    }

    Statement::Block(BlockStatement::new(statements))
}
*/


fn parse_var_decl_statement<'a, I: Iterator<Item=&'a str>>(iter: &mut Peekable<I>) -> Statement {
    if let Ok((ctype, Some(identifier))) = crate::ctype::parser::parse_with_identifier_and_subscripts(iter) {
        let initialization = match iter.next() {
            Some(";") => None,
            Some("=") => {
                let expression_tokens = iter.take_while(|&token| token != ";");
                Some(fragment_list_into_expression(expression_tokens))
            }
            _ => panic!("invalid token after variable declaration"),
        };
        Statement::VariableDeclaration(identifier, ctype, initialization)
    } else {
        panic!("no identifier given to variable declaration");
    }
}


fn parse_return_statement<'a, I: Iterator<Item=&'a str>>(iter: &mut Peekable<I>) -> Statement {
    let return_token = iter.next();
    assert_eq!(return_token, Some("return"));
    let expression_tokens: Vec<_> = iter.take_while(|&token| token != ";").collect();
    let expression = if expression_tokens.len() == 0 {
        None
    } else {
        Some(fragment_list_into_expression(expression_tokens))
    };
    Statement::Return(expression)
}


fn parse_if_statement<'a, I: Iterator<Item=&'a str>>(iter: &mut Peekable<I>) -> Statement {
    let opening_if = iter.next();
    assert_eq!(opening_if, Some("if"));
    let conditional_token_list = parse_parenthesis(iter);
    let conditional_expression = fragment_list_into_expression(conditional_token_list);

    let true_body = parse_statement(iter);

    let false_body = {
        if let Some(&"else") = iter.peek() {
            iter.next();
            Some(parse_statement(iter))
        } else {
            None
        }
    };

    Statement::IfElse(conditional_expression, box true_body, false_body.map(Box::new))
}


fn parse_while_statement<'a, I: Iterator<Item=&'a str>>(iter: &mut Peekable<I>) -> Statement {
    let opening_while = iter.next();
    assert_eq!(opening_while, Some("while"));
    let conditional_token_list = parse_parenthesis(iter);
    let conditional_expression = fragment_list_into_expression(conditional_token_list);

    let loop_body = parse_statement(iter);

    Statement::WhileLoop(conditional_expression, box loop_body)
}

