
use std::iter::Peekable;

use thiserror::Error;

#[derive(Debug, Error)]
pub enum ParseError {
    #[error("Parse Error: {0}")]
    Message(String),
    #[error("Parse Int Error: {0}")]
    ParseInt(#[from] std::num::ParseIntError),
}

impl ParseError {
    pub fn new<S: Into<String>>(msg: S) -> ParseError {
        ParseError::Message(msg.into())
    }
}


/// General trait for parsing different C constructs
pub trait Parsable: Sized {
    fn parse<'a, I: Iterator<Item=&'a str>>(iter: &mut Peekable<I>) -> Result<Self, ParseError>;
}

