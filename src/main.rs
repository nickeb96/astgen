
#![feature(box_syntax, box_patterns, or_patterns, optin_builtin_traits, negative_impls, bindings_after_at)]

#![allow(unused)]


extern crate tokenizer;
extern crate indextree;
extern crate regex;

use std::iter::FromIterator;

mod ctype;
mod expression;
mod statement;
mod declaration;
mod identifier;
mod utils;
mod parsable;
mod indented_display;
mod definition;
mod file_parser;

struct Thing(crate::ctype::cfunction::CFunction, crate::identifier::Identifier);

impl std::fmt::Display for Thing {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        self.0.display_with_identifier(&self.1, f)
    }
}

use parsable::*;
use expression::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let s = "
    struct person {
        short age;
        unsigned int ssn;
        short int favorite_numbers[3];
        char middle_initial;
        char* name;
        signed int height;
    }

    int triple_sum(int a, int b, int c) {
        return a + b + c;
    }

    void string_copy(char* src, char* dst) {
        while (*dst++ = *src++) { }
    }

    int factorial(int n) {
        int x = 1;
        int y = 2;
        while (y <= n) {
            x = x * y;
            ++y;
        }
        return y;
    }
    ";

    let s = "
    int f() {
        int x = 8;
        x[3] = 9;
        return;
    }
    ";

    let definitions = file_parser::parse_file(s).unwrap();

    for def in definitions.into_iter() {
        println!("{}", def);
    }


    Ok(())
}

