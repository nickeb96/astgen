
use crate::parsable::{Parsable, ParseError};
use crate::ctype::cstruct::CStruct;
use crate::ctype::cfunction::CFunction;
use crate::statement::block_statement::BlockStatement;
use crate::definition::Definition;


pub fn parse_file(source: &str) -> Result<Vec<Definition>, ParseError> {
    let tokens = tokenizer::iter_tokens(source);
    let mut token_iter = tokens.peekable();
    let mut ret = Vec::new();

    while let Some(token) = token_iter.peek() {
        let definition = match token {
            &"struct" => Definition::Struct(CStruct::parse(&mut token_iter)?),
            // assume function for now
            _ => {
                let (cfunction, maybe_identifier) = CFunction::parse_with_identifier(&mut token_iter)?;
                let identifier = maybe_identifier.ok_or_else(
                    || ParseError::new("function definition missing identifier"))?;
                let block_statement = BlockStatement::parse(&mut token_iter)?;
                Definition::Function(identifier, cfunction, block_statement)
            }
        };
        ret.push(definition);
    }

    Ok(ret)
}

