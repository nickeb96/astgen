
pub enum<Decl, Def> DeclarationOrDefinition {
    Declaration(Decl),
    Definition(Def),
}

