
use std::collections::HashSet;

use regex::Regex;
use lazy_static::lazy_static;
use maplit::hashset;

use crate::parsable::{Parsable, ParseError};


lazy_static! {
    static ref IDENTIFIER_RE: Regex = Regex::new(r"^[[:alpha:]_][[:alnum:]_]*$").unwrap();
    static ref KEYWORDS: HashSet<&'static str> = hashset!{
        "void", "char", "int", "float", "double", "signed", "unsigned",
        "short", "long", "const", "static", "enum", "struct", "union",
        "typedef", "if", "else", "switch", "case", "while", "do", "for",
        "break", "continue", "extern", "return", "sizeof", "alignof",
    };
}


#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct Identifier {
    name: String,
}

impl Identifier {
    pub fn new<S: Into<String>>(name: S) -> Result<Identifier, ParseError> {
        let name = name.into();
        if Identifier::is_alphanum(&name) {
            if !Identifier::is_keyword(&name) {
                Ok(Identifier { name })
            } else {
                Err(ParseError::new(format!("identifier '{:?}' is a keyword", name)))
            }
        } else {
            Err(ParseError::new(format!("identifier '{:?}' is not alphanumeric", name)))
        }
    }

    pub fn as_str(&self) -> &str {
        &self.name
    }

    pub fn is_identifier(string: &str) -> bool {
        Self::is_alphanum(string) && !Self::is_keyword(string)
    }

    pub fn is_alphanum(string: &str) -> bool {
        IDENTIFIER_RE.is_match(string)
    }

    pub fn is_keyword(string: &str) -> bool {
        KEYWORDS.contains(string)
    }
}

impl std::fmt::Display for Identifier {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.name)
    }
}
