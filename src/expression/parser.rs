
use regex::Regex;

use crate::expression::Expression;
use crate::expression::operator::{PreOp, PostOp, BinOp, Operator};
use crate::identifier::Identifier;

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Fragment<'a> {
    RawToken(&'a str),
    PartialExpr(Expression),
}
use Fragment::*;


impl<'a> From<&'a str> for Fragment<'a> {
    fn from(s: &'a str) -> Fragment<'a> {
        Fragment::RawToken(s)
    }
}


impl<'a> Fragment<'a> {
    pub fn into_raw_token(self) -> &'a str {
        match self {
            RawToken(s) => s,
            _ => panic!("Fragment is not a RawToken."),
        }
    }

    pub fn into_expression(self) -> Expression {
        match self {
            PartialExpr(expr) => expr,
            _ => panic!("Fragment is not a PartialExpr."),
        }
    }

    pub fn is_raw_token(&self) -> bool {
        match self {
            RawToken(_) => true,
            _ => false,
        }
    }

    pub fn is_expression(&self) -> bool {
        match self {
            PartialExpr(_) => true,
            _ => false,
        }
    }
}


fn parse_leaves(tokens: &mut Vec<Fragment>) {
    let intlit_re = Regex::new(r"[0-9]+").unwrap();

    for i in 0..tokens.len() {
        if let RawToken(s) = tokens[i] {
            if let Ok(ident) = Identifier::new(s) {
                tokens[i] = PartialExpr(Expression::Identifier(ident));
            } else if intlit_re.is_match(s) {
                tokens[i] = PartialExpr(Expression::IntegerLiteral(String::from(s)));
            }
        }
    }
}

/// Move left to right starting with the second element.  For each frament
/// that is still a raw token, check to see if the token is one of the valid
/// post-ops and that the fragment preceding it is an experession.  If so,
/// remove the preceding expression and then replace the fragment with the
/// post-op and make the removed expression a child of the newly created
/// expression.  Special postfix operators like field access and subscript
/// have more complex logic.
fn parse_postfix_operators(tokens: &mut Vec<Fragment>) {
    let mut i = 1;
    while i < tokens.len() {
        if let RawToken(token) = tokens[i] {
            if let Some(op) = PostOp::from_token(token) {
                if tokens[i - 1].is_expression() {
                    let child = tokens.remove(i - 1).into_expression();
                    i -= 1;
                    tokens[i] = PartialExpr(Expression::PostfixOperator(op, box child));
                }
            } else if token == "." && matches!(tokens.get(i - 1), Some(&PartialExpr(_))) {
                match tokens.get(i + 1) {
                    Some(PartialExpr(Expression::Identifier(ident))) => {
                        // remove identifier from token list
                        let ident_expr = tokens.remove(i + 1).into_expression();
                        let ident = match ident_expr {
                            Expression::Identifier(ident) => ident,
                            _ => unreachable!(),
                        };
                        let base_expr = tokens.remove(i - 1).into_expression();
                        i -= 1;
                        // replace expression before dot with FieldAccess
                        tokens[i] = PartialExpr(Expression::PostfixOperator(
                                PostOp::FieldAccess(ident), box base_expr));
                    }
                    _ => (),
                }
            } else if token == "["
                && matches!(tokens.get(i - 1), Some(&PartialExpr(_)))
                && matches!(tokens.get(i + 1), Some(&PartialExpr(_)))
                && matches!(tokens.get(i + 2), Some(&RawToken("]")))
            {
                tokens.remove(i + 2); // remove closing square bracket
                let index = tokens.remove(i + 1).into_expression(); // take inner index expression
                let base = tokens.remove(i - 1).into_expression(); // take base expression
                i -= 1;
                // replace opening square bracket with subscript expression
                tokens[i] = PartialExpr(Expression::PostfixOperator(
                        PostOp::Subscript(box index), box base));
            }
        }
        i += 1;
    }
}

// right to left
fn parse_prefix_operators(tokens: &mut Vec<Fragment>) {
    let mut i = tokens.len() as isize - 2;
    while i >= 0 {
        if let RawToken(token) = tokens[i as usize] {
            if let Some(op) = PreOp::from_token(token) {
                if tokens[i as usize + 1].is_expression() && (i < 1 || !tokens[i as usize - 1].is_expression()) {
                    let child = tokens.remove(i as usize + 1).into_expression();
                    tokens[i as usize] = PartialExpr(Expression::PrefixOperator(op, box child));
                }
            }
        }
        i -= 1;
    }
}

fn parse_multiplicative_operators(tokens: &mut Vec<Fragment>) {
    let mut i = 1;
    while i < tokens.len() - 1 {
        if let RawToken(token) = tokens[i] {
            match token {
                "*" | "/" | "%" => {
                    if tokens[i - 1].is_expression() && tokens[i + 1].is_expression() {
                        let right = tokens.remove(i + 1).into_expression();
                        let left = tokens.remove(i - 1).into_expression();
                        i -= 1;
                        tokens[i] = PartialExpr(Expression::BinaryOperator(BinOp::from_token(token).unwrap(), box left, box right));
                    }
                }
                _ => ()
            }
        }
        i += 1;
    }
}

fn parse_additive_operators(tokens: &mut Vec<Fragment>) {
    let mut i = 1;
    while i < tokens.len() - 1 {
        if let RawToken(token) = tokens[i] {
            match token {
                "+" | "-" => {
                    if tokens[i - 1].is_expression() && tokens[i + 1].is_expression() {
                        let right = tokens.remove(i + 1).into_expression();
                        let left = tokens.remove(i - 1).into_expression();
                        i -= 1;
                        tokens[i] = PartialExpr(Expression::BinaryOperator(BinOp::from_token(token).unwrap(), box left, box right));
                    }
                }
                _ => ()
            }
        }
        i += 1;
    }
}

fn parse_comparison_operators(tokens: &mut Vec<Fragment>) {
    let mut i = 1;
    while i < tokens.len() - 1 {
        if let RawToken(token) = tokens[i] {
            match token {
                "<" | "<=" | ">" | ">=" => {
                    if tokens[i - 1].is_expression() && tokens[i + 1].is_expression() {
                        let right = tokens.remove(i + 1).into_expression();
                        let left = tokens.remove(i - 1).into_expression();
                        i -= 1;
                        tokens[i] = PartialExpr(Expression::BinaryOperator(BinOp::from_token(token).unwrap(), box left, box right));
                    }
                }
                _ => ()
            }
        }
        i += 1;
    }
}

fn parse_equality_operators(tokens: &mut Vec<Fragment>) {
    let mut i = 1;
    while i < tokens.len() - 1 {
        if let RawToken(token) = tokens[i] {
            match token {
                "==" | "!=" => {
                    if tokens[i - 1].is_expression() && tokens[i + 1].is_expression() {
                        let right = tokens.remove(i + 1).into_expression();
                        let left = tokens.remove(i - 1).into_expression();
                        i -= 1;
                        tokens[i] = PartialExpr(Expression::BinaryOperator(BinOp::from_token(token).unwrap(), box left, box right));
                    }
                }
                _ => ()
            }
        }
        i += 1;
    }
}

fn parse_assignment_operators(tokens: &mut Vec<Fragment>) {
    let mut i: isize = tokens.len() as isize - 2;
    while i > 0 {
        if let RawToken(token) = tokens[i as usize] {
            match token {
                "=" => {
                    if tokens[i as usize - 1].is_expression() && tokens[i as usize + 1].is_expression() {
                        let right = tokens.remove(i as usize + 1).into_expression();
                        let left = tokens.remove(i as usize - 1).into_expression();
                        i -= 1;
                        tokens[i as usize] = PartialExpr(Expression::BinaryOperator(BinOp::from_token(token).unwrap(), box left, box right));
                    }
                }
                _ => ()
            }
        }
        i -= 1;
    }
}

/// Fully parse all parenthesized expression into a single AST
fn parse_parenthesis(tokens: &mut Vec<Fragment>) {
    let mut i = 0;
    while i < tokens.len() {
        if tokens[i] == RawToken("(") {
            let mut sub_list = Vec::new();
            let mut parenthesis_depth = 1;
            tokens.remove(i); // remove opening parenthesis
            // collect parenthesized expression into `sub_list`
            loop {
                assert!(i < tokens.len(), "Parenthesized expression missing closing parenthesis");
                match tokens[i] {
                    RawToken("(") => parenthesis_depth += 1,
                    RawToken(")") => parenthesis_depth -= 1,
                    _ => ()
                }

                if parenthesis_depth > 0 {
                    sub_list.push(tokens.remove(i));
                }
                else {
                    tokens.remove(i); // remove closing palenthesis
                    break;
                }
            }
            tokens.insert(i, PartialExpr(fragment_list_into_expression(sub_list)));
        }
        i += 1;
    }
}

/// Similar to `parse_parenthesis` but leave the square brackets in the token
/// list to allow subscript parser to detect the index.
fn parse_square_brackets(tokens: &mut Vec<Fragment>) {
    let mut i = 0;
    while i < tokens.len() {
        if tokens[i] == RawToken("[") {
            let mut sub_list = Vec::new();
            let mut depth = 1;
            i += 1; // skip over opening square bracket
            // collect expression into `sub_list`
            loop {
                assert!(i < tokens.len(), "Square bracketed expression missing closing bracket");
                match tokens[i] {
                    RawToken("[") => depth += 1,
                    RawToken("]") => depth -= 1,
                    _ => ()
                }

                if depth > 0 {
                    sub_list.push(tokens.remove(i));
                }
                else {
                    break;
                }
            }
            tokens.insert(i, PartialExpr(fragment_list_into_expression(sub_list)));
        }
        i += 1;
    }
}

/// Reduce a token list into one root expression by parsing operators in the
/// order of their precedence and the direction of their associativity.
pub fn fragment_list_into_expression<'a, I, T>(iter: I) -> Expression
where
    I: IntoIterator<Item=T>,
    T: Into<Fragment<'a>>,
{
    let mut tokens = iter.into_iter().map(|s| s.into()).collect();

    parse_parenthesis(&mut tokens);
    parse_square_brackets(&mut tokens);
    parse_leaves(&mut tokens);
    parse_postfix_operators(&mut tokens);
    parse_prefix_operators(&mut tokens);
    parse_multiplicative_operators(&mut tokens);
    parse_additive_operators(&mut tokens);
    parse_comparison_operators(&mut tokens);
    parse_equality_operators(&mut tokens);
    parse_assignment_operators(&mut tokens);

    assert!(tokens.len() == 1);

    tokens.remove(0).into_expression()
}

