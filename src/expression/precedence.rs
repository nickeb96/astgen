#![allow(unused)]


/// The values for operator precedence are mostly arbitrary.  The actual
/// values do not matter, only that the order (i.e. Multiplicative is higher
/// than Additive).  There is a lot of space in between for flexibility later
/// on.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Precedence {
    Literal = 201,
    Identifier = 200,
    Postfix = 150,
    Prefix = 140,
    Multiplicative = 130,
    Additive = 120,
    BitShift = 110,
    Comparative = 100,
    Equality = 90,
    BitAnd = 86,
    BitXor = 84,
    BitOr = 82,
    LogicalAnd = 76,
    LogicalOr = 74,
    Ternary = 60,
    Assignment = 50,
}

