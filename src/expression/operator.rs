
use crate::identifier::Identifier;
use crate::expression::Expression;
use super::precedence::Precedence;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Associativity {
    LeftToRight,
    RightToLeft,
}

pub trait Operator {
    fn from_token(token: &str) -> Option<Self> where Self: Sized;
    fn as_str(&self) -> &'static str;
    fn precedence(&self) -> Precedence;
    fn associativity(&self) -> Associativity;
}

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum PostOp {
    Inc,
    Dec,
    FieldAccess(Identifier),
    Subscript(Box<Expression>),
    FnCall,
}

impl Operator for PostOp {
    fn from_token(token: &str) -> Option<PostOp> {
        use PostOp::*;
        match token {
            "++" => Some(Inc),
            "--" => Some(Dec),
            _ => None,
        }
    }

    fn as_str(&self) -> &'static str {
        use PostOp::*;
        match self {
            Inc => "++",
            Dec => "--",
            FieldAccess(_) => ".",
            Subscript(_) => "[]",
            _ => todo!(),
        }
    }

    fn precedence(&self) -> Precedence {
        Precedence::Postfix
    }

    fn associativity(&self) -> Associativity {
        Associativity::LeftToRight
    }
}


#[derive(Debug, Eq, PartialEq, Clone)]
pub enum PreOp {
    Inc,
    Dec,
    Promote,
    Neg,
    Not,
    BitNot,
    Deref,
    Addr,
}

impl Operator for PreOp {
    fn from_token(token: &str) -> Option<PreOp> {
        use PreOp::*;
        match token {
            "++" => Some(Inc),
            "--" => Some(Dec),
            "+" => Some(Promote),
            "-" => Some(Neg),
            "!" => Some(Not),
            "~" => Some(BitNot),
            "*" => Some(Deref),
            "&" => Some(Addr),
            _ => None,
        }
    }

    fn as_str(&self) -> &'static str {
        use PreOp::*;
        match self {
            Inc => "++",
            Dec => "--",
            Promote => "+",
            Neg => "-",
            Not => "!",
            BitNot => "~",
            Deref => "*",
            Addr => "&",
        }
    }

    fn precedence(&self) -> Precedence {
        Precedence::Prefix
    }

    fn associativity(&self) -> Associativity {
        Associativity::RightToLeft
    }
}


#[derive(Debug, Eq, PartialEq, Clone)]
pub enum BinOp {
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    LessThan,
    LessEqual,
    GreaterThan,
    GreaterEqual,
    Equal,
    NotEqual,
    And,
    Or,
    Assign,
    BitLeftShift,
    BitRightShift,
    BitAnd,
    BitOr,
    BitXor,
}

impl Operator for BinOp {
    fn from_token(token: &str) -> Option<BinOp> {
        use BinOp::*;
        match token {
            "+" => Some(Add),
            "-" => Some(Sub),
            "*" => Some(Mul),
            "/" => Some(Div),
            "%" => Some(Mod),
            "<" => Some(LessThan),
            "<=" => Some(LessEqual),
            ">" => Some(GreaterThan),
            ">=" => Some(GreaterEqual),
            "==" => Some(Equal),
            "!=" => Some(NotEqual),
            "&&" => Some(And),
            "||" => Some(Or),
            "=" => Some(Assign),
            "<<" => Some(BitLeftShift),
            ">>" => Some(BitRightShift),
            "&" => Some(BitAnd),
            "|" => Some(BitOr),
            "^" => Some(BitXor),
            _ => None,
        }
    }

    fn as_str(&self) -> &'static str {
        use BinOp::*;
        match self {
            Add => "+",
            Sub => "-",
            Mul => "*",
            Div => "/",
            Mod => "%",
            LessThan => "<",
            LessEqual => "<=",
            GreaterThan => ">",
            GreaterEqual => ">=",
            Equal => "==",
            NotEqual => "!=",
            And => "&&",
            Or => "||",
            Assign => "=",
            BitLeftShift => "<<",
            BitRightShift => ">>",
            BitAnd => "&",
            BitOr => "|",
            BitXor => "^",
        }
    }

    fn precedence(&self) -> Precedence {
        use BinOp::*;
        match self {
            Add | Sub => Precedence::Additive,
            Mul | Div | Mod => Precedence::Multiplicative,
            LessThan | LessEqual | GreaterThan | GreaterEqual => Precedence::Comparative,
            Equal | NotEqual => Precedence::Equality,
            And => Precedence::LogicalAnd,
            Or => Precedence::LogicalOr,
            Assign => Precedence::Assignment,
            BitLeftShift | BitRightShift => Precedence::BitShift,
            BitAnd => Precedence::BitAnd,
            BitOr => Precedence::BitOr,
            BitXor => Precedence::BitXor,
        }
    }

    fn associativity(&self) -> Associativity {
        use BinOp::*;
        match self {
            Assign => Associativity::RightToLeft,
            _ => Associativity::LeftToRight,
        }
    }
}

