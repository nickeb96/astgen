use regex::Regex;

use crate::ctype::CType;
use crate::ctype::cinteger::{CInteger, Signedness};

use super::operator::{PreOp, PostOp, BinOp, Operator};
use super::precedence::Precedence;

#[derive(Debug, Eq, PartialEq, Clone)]
pub enum Expression {
    IntegerLiteral(String),
    Identifier(crate::identifier::Identifier),
    PrefixOperator(PreOp, Box<Expression>),
    PostfixOperator(PostOp, Box<Expression>),
    BinaryOperator(BinOp, Box<Expression>, Box<Expression>),
}

impl Expression {
    /// Return precedence level of the root of this expression
    fn precedence(&self) -> Precedence {
        use Expression::*;
        match self {
            IntegerLiteral(_) => Precedence::Literal,
            Identifier(_) => Precedence::Identifier,
            PrefixOperator(op, _) => op.precedence(),
            PostfixOperator(op, _) => op.precedence(),
            BinaryOperator(op, _, _) => op.precedence(),
        }
    }

    /// Determine if this expression can be used as an L-value
    ///
    /// Examples:
    /// ```c
    ///   x = 7;  // x can be used as an lvalue
    ///   7 = x;  // 7 cannot be used as an lvalue
    ///   x + y = 7;  // sum of x and y cannot be used as an lvalue
    ///   *(x + y) = 7;  // dereference can be used as an lvalue
    /// ```
    pub fn is_lvalue(&self) -> bool {
        use Expression::*;
        match self {
            IntegerLiteral(_) => false,
            Identifier(_) => true,
            PrefixOperator(op, child) => {
                match op {
                    PreOp::Deref => true,
                    _ => false,
                }
            }
            PostfixOperator(op, child) => {
                match op {
                    PostOp::Subscript(_) | PostOp::FieldAccess(_) => true,
                    PostOp::Inc | PostOp::Dec | PostOp::FnCall => false,
                }
            }
            BinaryOperator(op, left, right) => false,
        }
    }

    // TODO: add error handling, ie. x = &7
    /*
    pub fn get_ctype(&self) -> CType {
        use Expression::*;
        match self {
            IntegerLiteral(_) => CType::Int(Sign::Signed),
            Identifier(_name) => todo!("you need to find a way to look up variable types"),
            PrefixOperator(op, child) => {
                match op {
                    PreOp::Deref => {
                        match child.get_ctype() {
                            CType::Pointer(box pointee_type) => pointee_type.clone(),
                            _ => todo!("this is an error"),
                        }
                    }
                    PreOp::Addr => {
                        if child.is_lvalue() {
                            CType::Pointer(box child.get_ctype())
                        } else {
                            todo!("this is an error")
                        }
                    }
                    PreOp::Not => CType::Int(Sign::Signed), // `!` always returns `int`
                    PreOp::Inc | PreOp::Dec | PreOp::Neg | PreOp::BitNot => child.get_ctype(),
                    PreOp::Promote => todo!("you need to figure out exactly how unary plus works"),
                }
            }
            PostfixOperator(op, child) => {
                match op {
                    PostOp::Inc | PostOp::Dec => child.get_ctype(),
                    PostOp::FnCall => todo!("same issue as identifier lookup"),
                    PostOp::FieldAccess => {
                        // TODO: finish this
                        let target_field_name = "example";
                        match child.get_ctype() {
                            CType::Struct(struct_name, fields) => {
                                fields.iter().find_map(|(name, ctype)| {
                                    if name == target_field_name {
                                        Some(ctype.clone())
                                    } else {
                                        None
                                    }
                                }).expect("field does not exist")
                            }
                            _ => todo!("this is an error"),
                        }
                    }
                    PostOp::Subscript => match child.get_ctype() {
                        CType::Ptr(box pointee_type) => pointee_type.clone(),
                        CType::Array(box subtype, _) => subtype.clone(),
                        _ => todo!("this is an error"),
                    }
                }
            }
            BinaryOperator(op, left, right) => {
                match op {
                    BinOp::Add | BinOp::Sub | BinOp::Mul | BinOp::Div | BinOp::Mod
                        | BinOp::BitOr | BinOp::BitXor | BinOp::BitAnd
                    => {
                        let left = left.get_ctype();
                        let right = right.get_ctype();
                        CType::determine_output(&left, &right)
                    }
                    BinOp::LessThan | BinOp::LessEqual | BinOp::GreaterThan | BinOp::GreaterEqual
                        | BinOp::Equal | BinOp::NotEqual => CType::Int(Sign::Signed),
                    BinOp::Assign => left.get_ctype(),
                    BinOp::And | BinOp::Or => unimplemented!(),
                    BinOp::BitLeftShift | BinOp::BitRightShift => left.get_ctype(),
                }
            }
        }
    }
    */
}


impl std::fmt::Display for Expression {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use Expression::*;
        match self {
            IntegerLiteral(value) => write!(f, "{}", value),
            Identifier(ident) => write!(f, "{}", ident),
            PrefixOperator(op, child) => write!(f, "{}{}", op.as_str(), child),
            PostfixOperator(op @ PostOp::FieldAccess(field), base) => {
                // insert parenthesis when necessary
                if base.precedence() < op.precedence() {
                    write!(f, "({}).{}", base, field)
                } else {
                    write!(f, "{}.{}", base, field)
                }
            }
            PostfixOperator(op @ PostOp::Subscript(index), base) => {
                if base.precedence() < op.precedence() {
                    write!(f, "({})[{}]", base, index)
                } else {
                    write!(f, "{}[{}]", base, index)
                }
            }
            PostfixOperator(op, child) => write!(f, "{}{}", child, op.as_str()),
            BinaryOperator(op, left, right) => {
                // insert parenthesis when necessary
                let left = if op.precedence() > left.precedence() {
                    format!("({})", left)
                } else {
                    left.to_string()
                };
                let right = if op.precedence() > right.precedence() {
                    format!("({})", right)
                } else {
                    right.to_string()
                };

                write!(f, "{} {} {}", left, op.as_str(), right)
            }
        }
    }
}

