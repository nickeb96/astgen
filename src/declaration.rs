
use std::iter::Peekable;
use crate::ctype::CType;
use crate::ctype::cinteger::{CInteger, Signedness};
use crate::ctype::cstruct::CStruct;
use crate::ctype::cfunction::CFunction;
use crate::statement::Statement;
use crate::identifier::Identifier;
use crate::parsable::Parsable;


#[derive(Debug)]
pub enum Declaration {
    Function(Identifier, CFunction),
    Struct(Identifier),
    GlobalVariable(Identifier, CType),
}

