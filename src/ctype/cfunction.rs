
use std::iter::Peekable;

use crate::ctype::CType;
use crate::parsable::{Parsable, ParseError};
use crate::identifier::Identifier;


#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct CFunction {
    return_ctype: CType,
    args: Vec<(Option<Identifier>, CType)>,
}

impl CFunction {
    pub fn display_with_identifier(&self, identifier: &Identifier, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} {}(", self.return_ctype, identifier)?;
        for (i, (maybe_identifier, ctype)) in self.args.iter().enumerate() {
            if let Some(identifier) = maybe_identifier {
                ctype.display_with_identifier(identifier, f)?;
            } else {
                write!(f, "{}", ctype)?;
            }
            if i != self.args.len() - 1 {
                write!(f, ", ")?;
            }
        }
        write!(f, ")")
    }

    pub fn return_ctype(&self) -> &CType {
        &self.return_ctype
    }

    pub fn iter_args(&self) -> impl Iterator<Item=&(Option<Identifier>, CType)> {
        self.args.iter()
    }

    pub fn parse_with_identifier<'a, I: Iterator<Item=&'a str>>(
        iter: &mut Peekable<I>
    ) -> Result<(CFunction, Option<Identifier>), ParseError> {
        let (return_ctype, maybe_identifier) =
            crate::ctype::parser::parse_with_identifier(iter)?;
        let arg_tokens = crate::statement::parser::parse_parenthesis(iter);
        let mut args = Vec::new();
        if arg_tokens.len() > 0 {
            for subslice in arg_tokens.split(|&s| s == ",") {
                let mut subslice_iter = subslice.into_iter().copied().peekable();
                let (arg_ctype, maybe_arg_name)
                    = crate::ctype::parser::parse_with_identifier_and_subscripts(&mut subslice_iter)?;
                if subslice_iter.next().is_some() {
                    return Err(ParseError::new("trailing tokens found after arg"));
                }
                args.push((maybe_arg_name, arg_ctype));
            }
        }
        Ok((CFunction { return_ctype, args }, maybe_identifier))
    }
}


/*
fn parse_function<'a, I: Iterator<Item=&'a str>>(iter: &mut Peekable<I>) -> Declaration {
    let return_type = crate::ctype::parser::parse_beginning(iter).unwrap();
    let function_name = iter.next().expect("end of file encountered mid function parse").to_owned();
    let function_name = Identifier::new(function_name).unwrap();
    let arg_tokens = crate::statement::parser::parse_parenthesis(iter);
    let mut args = Vec::new();
    for subslice in arg_tokens.split(|&s| s == ",") {
        let mut subslice_iter = subslice.into_iter().copied().peekable();
        let (arg_type, arg_name) = crate::ctype::parser::parse_with_identifier(&mut subslice_iter).unwrap();
        assert!(subslice_iter.next() == None);
        args.push((arg_name.unwrap(), arg_type));
    }
    let body = crate::statement::parser::parse_statement(iter);
    Declaration::Function(function_name, return_type, args, body)
}

*/
