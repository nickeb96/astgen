
pub mod parser;
pub mod cstruct;
pub mod cinteger;
pub mod cfunction;
pub mod concrete_ctype;


use std::iter::Peekable;

use crate::identifier::Identifier;
use cinteger::CInteger;
use cstruct::CStruct;



/// Generic enum encompassing all C types
#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum CType {
    Void,
    Integer(CInteger),
    // FloatingPoint
    Pointer(Box<CType>),
    Array(Box<CType>, Option<usize>),
    Struct(CStruct),
}


impl CType {
    pub fn is_integer(&self) -> bool {
        match self {
            CType::Integer(_) => true,
            _ => false,
        }
    }

    pub fn display_with_identifier(&self, identifier: &Identifier, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use CType::*;
        match self {
            Void => write!(f, "void {}", identifier),
            Integer(cinteger) => write!(f, "{} {}", cinteger, identifier),
            Pointer(pointee) => write!(f, "{}* {}", pointee, identifier),
            Array(element_ctype, Some(length)) => write!(f, "{} {}[{}]", element_ctype, identifier, length),
            Array(element_ctype, None) => write!(f, "{} {}[]", identifier, element_ctype),
            Struct(cstruct) => write!(f, "{} {}", cstruct, identifier),
        }
    }

    /*
    /// This function is based on a complex set of integer promotion rules
    /// defined in Section 6.3.1 of the C11 standard.
    pub fn determine_output(lhs: &Self, rhs: &Self) -> CType {
        use CType::*;
        use Sign::*;
        match (lhs, rhs) {
            // Binary operations on integers and pointers (which arrays can be implicitly
            // coerced into) always result in a pointer.
            (left @ (Ptr(_) | Array(_, _)), right) if right.is_integer() => left.clone(),
            (left, right @ (Ptr(_) | Array(_, _))) if left.is_integer() => right.clone(),
            // If one operand is `int` and the other is lower ranked than
            // `int`, the resulting type is `int` carrying the signed/unsigned
            // qualifier from the `int` operand.
            (Char(_) | Short(_), Int(sign)) => Int(*sign),
            (Int(sign), Char(_) | Short(_)) => Int(*sign),
            // If both operands are lower rank than `int` and `unsigned int`,
            // the result is always a signed `int`, regardless of operand sign.
            (Char(_) | Short(_), Char(_) | Short(_)) => Int(Signed),
            // If both operands are `int`, but one is signed and the other is
            // unsigned, the result is unsigned.
            (Int(Signed), Int(Unsigned)) | (Int(Unsigned), Int(Signed)) => Int(Unsigned),
            // No changes to homogenous operands of `int`s.
            (Int(Signed), Int(Signed)) => Int(Signed),
            (Int(Unsigned), Int(Unsigned)) => Int(Unsigned),
            // Operations involving long promote the other integer operand to
            // a `long` of the same sign as the long.
            (Long(sign), Char(_) | Short(_) | Int(_)) => Long(*sign),
            (Char(_) | Short(_) | Int(_), Long(sign)) => Long(*sign),
            // `long` follows the same rules as `int` for operands only
            // including `long` or `unsigned long`.
            (Long(Signed), Long(Unsigned)) | (Long(Unsigned), Long(Signed)) => Long(Unsigned),
            (Long(Signed), Long(Signed)) => Long(Signed),
            (Long(Unsigned), Long(Unsigned)) => Long(Unsigned),
            // If one operand is any floating-point type and the other is an
            // integer type, the result will be a floating-point type with the
            // same precision as the floating-point operand.
            (left @ (Float | Double), right) if right.is_integer() => left.clone(),
            (left, right @ (Float | Double)) if left.is_integer() => right.clone(),
            // The only valid binary operator for structs is to assign from one
            // to the other, and they must be the same type.
            (left @ Struct(_, _), right @ Struct(_, _)) if left == right => left.clone(),
            _ => panic!("Invalid operation between types"), // TODO: return error instead
        }
    }*/
}


pub fn is_type(token: &str) -> bool {
    match token {
        "void" | "unsigned" | "signed" | "char" | "short" | "int" | "long" | "float" | "double" | "struct" => true,
        _ => false,
    }
}


impl std::fmt::Display for CType {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use CType::*;
        match self {
            Void => write!(f, "void"),
            Integer(cinteger) => write!(f, "{}", cinteger),
            Pointer(pointee) => write!(f, "{}*", pointee),
            Array(element_ctype, Some(length)) => write!(f, "{}[{}]", element_ctype, length),
            Array(element_ctype, None) => write!(f, "{}[]", element_ctype),
            Struct(cstruct) => write!(f, "{}", cstruct),
        }
    }
}

