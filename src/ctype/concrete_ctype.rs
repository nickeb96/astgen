
use crate::ctype::CType;


pub struct ConcreteCType {
    ctype: CType,
}

impl ConcreteCType {
    pub fn new(ctype: CType) -> Result<ConcreteCType, ()> {
        match ctype {
            CType::Array(_, None) => Err(()),
            other => Ok(ConcreteCType { ctype: other }),
        }
    }
}
