
use std::iter::Peekable;
use crate::identifier::Identifier;
use super::CType;
use super::cinteger::{CInteger, Signedness};
use super::cstruct::CStruct;
use crate::parsable::{Parsable, ParseError};


pub fn is_type(token: &str) -> bool {
    match token {
        "void" | "unsigned" | "signed" | "char" | "short" | "int" | "long" | "float" | "double" | "struct" => true,
        _ => false,
    }
}

/// Parse beginning of a type
///
/// Will not parse pointers or anything left of a pointer.
pub fn parse_beginning<'a, I: Iterator<Item=&'a str>>(
    iter: &mut Peekable<I>
) -> Result<CType, ParseError> {
    use Signedness::*;

    let signedness: Option<Signedness> = iter.peek().and_then(|token| token.parse().ok());
    if signedness.is_some() {
        iter.next(); // consume signedness token
    }

    // TODO: cleanup and don't store str literals
    let width = match iter.peek() {
        None => return Err(ParseError::new("ran out of tokens while parsing type")),
        Some(&"short") => {
            iter.next();
            Some("short")
        }
        Some(&"long") => {
            iter.next();
            Some("long")
        }
        _ => None,
    };

    match iter.peek() {
        Some(&"void") if signedness.is_none() && width.is_none() => {
            iter.next();
            Ok(CType::Void)
        }
        Some(&"void") if signedness.is_some() || width.is_some() => {
            Err(ParseError::new("void type cannot have qualifiers"))
        }
        Some(&"char") if width.is_none() => {
            iter.next();
            Ok(CType::Integer(CInteger::Char(signedness.unwrap_or(Signed))))
        }
        Some(&"char") if width.is_some() => {
            Err(ParseError::new("width qualifier not allowed on char"))
        }
        Some(&"int") => {
            iter.next();
            let sign = signedness.unwrap_or(Signed);
            match width {
                Some("short") => Ok(CType::Integer(CInteger::Short(sign))),
                Some("long") => Ok(CType::Integer(CInteger::Long(sign))),
                None => Ok(CType::Integer(CInteger::Int(sign))),
                _ => unreachable!(),
            }
        }
        _ if signedness.is_some() || width.is_some() => {
            // if no "base type" is given, then use int as default; this only
            // works if at least a sign or width is given
            let sign = signedness.unwrap_or(Signed);
            match width {
                Some("short") => Ok(CType::Integer(CInteger::Short(sign))),
                Some("long") => Ok(CType::Integer(CInteger::Long(sign))),
                None => Ok(CType::Integer(CInteger::Int(sign))),
                _ => unreachable!(),
            }
        }
        other => Err(ParseError::new(format!("'{:?}' is not a type", other))),
    }
}

/// Parse everything up to the identifier
///
/// Will not parse the identifier or anything after it.
pub fn parse_beginning_with_pointers<'a, I: Iterator<Item=&'a str>>(
    iter: &mut Peekable<I>,
) -> Result<CType, ParseError> {
    let mut base_type = parse_beginning(iter)?;

    // handle pointers
    while let Some(&token) = iter.peek() {
        match token {
            "*" => {
                base_type = CType::Pointer(box base_type);
                iter.next(); // consume asterisk
            }
            _ => break,  // done parsing pointers
        }
    }

    Ok(base_type)
}

/// Parse a full type and an optional identifier
///
/// Will not parse array subscripts.
pub fn parse_with_identifier<'a, I: Iterator<Item=&'a str>>(
    iter: &mut Peekable<I>,
) -> Result<(CType, Option<Identifier>), ParseError> {
    let base_type = parse_beginning_with_pointers(iter)?;
    let mut identifier = None;

    // optional identifier
    if let Some(&token) = iter.peek() {
        if let Ok(ident) = Identifier::new(token) {
            identifier = Some(ident);
            iter.next();
        }
    }

    Ok((base_type, identifier))
}


/// Parse a full type with its optional identifier and trailling array notation
pub fn parse_with_identifier_and_subscripts<'a, I: Iterator<Item=&'a str>>(
    iter: &mut Peekable<I>,
) -> Result<(CType, Option<Identifier>), ParseError> {
    let (mut base_type, identifier) = parse_with_identifier(iter)?;

    while let Some(&token) = iter.peek() {
        if token == "[" {
            iter.next(); // consume opening bracket

            let array_length = {
                match iter.next() {
                    Some("]") => None,
                    Some(next_token) => {
                        let val = next_token.parse()?;
                        match iter.next() {
                            Some("]") => (),
                            other => return Err(ParseError::new(format!("expected end of array, but found {:?}", other))),
                        }
                        Some(val)
                    }
                    other => return Err(ParseError::new(format!("expected array length or end of array, but found {:?}", other))),
                }
            };

            base_type = CType::Array(box base_type, array_length);
        } else {
            break;
        }
    }

    Ok((base_type, identifier))
}
