

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct InvalidSignQualifier;

impl std::fmt::Display for InvalidSignQualifier {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "not a signedness type qualifier")
    }
}

impl std::error::Error for InvalidSignQualifier {}


#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Signedness {
    Signed,
    Unsigned,
}

impl std::str::FromStr for Signedness {
    type Err = InvalidSignQualifier;

    fn from_str(s: &str) -> Result<Self, InvalidSignQualifier> {
        use Signedness::*;
        match s {
            "signed" => Ok(Signed),
            "unsigned" => Ok(Unsigned),
            _ => Err(InvalidSignQualifier)
        }
    }
}

impl std::fmt::Display for Signedness {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use Signedness::*;
        match self {
            Signed => write!(f, "signed"),
            Unsigned => write!(f, "unsigned"),
        }
    }
}


#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub enum CInteger {
    Char(Signedness),
    Short(Signedness),
    Int(Signedness),
    Long(Signedness),
}

impl std::fmt::Display for CInteger {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        use CInteger::*;
        use Signedness::*;
        write!(f , "{}", match self {
            Char(Signed) => "char",
            Char(Unsigned) => "unsigned char",
            Short(Signed) => "short",
            Short(Unsigned) => "unsigned short",
            Int(Signed) => "int",
            Int(Unsigned) => "unsigned int",
            Long(Signed) => "long",
            Long(Unsigned) => "unsigned long",
        })
    }
}

