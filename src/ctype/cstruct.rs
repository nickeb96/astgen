
use std::iter::Peekable;

use crate::identifier::Identifier;
use crate::ctype::CType;
use crate::parsable::{Parsable, ParseError};
use crate::indented_display::{indent_text, BASE_INDENT_STR};


#[derive(Debug, Clone, Eq, PartialEq, Hash)]
pub struct CStruct {
    name: Identifier,
    fields: Vec<(Identifier, CType)>,
}

impl CStruct {
    pub fn new(name: Identifier, fields: Vec<(Identifier, CType)>) -> Self {
        Self { name, fields }
    }

    pub fn name(&self) -> &Identifier {
        &self.name
    }

    pub fn fields(&self) -> &[(Identifier, CType)] {
        &self.fields
    }
}

impl std::fmt::Display for CStruct {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        writeln!(f, "struct {} {{", self.name)?;
        for (field_name, field_type) in self.fields.iter() {
            f.write_str(BASE_INDENT_STR)?;
            field_type.display_with_identifier(field_name, f)?;
            writeln!(f, ";")?;
        }
        write!(f, "}};")
    }
}


impl Parsable for CStruct {
    fn parse<'a, I: Iterator<Item=&'a str>>(iter: &mut Peekable<I>) -> Result<Self, ParseError> {
        let struct_token = iter.next();
        if struct_token != Some("struct") {
            return Err(ParseError::new(format!("invalid token '{:?}', expected 'struct'", struct_token)));
        }
        let struct_name = iter.next().and_then(|s| Identifier::new(s).ok());
        // TODO: remove this line and make structs able to be anonymous
        let struct_name = struct_name.ok_or_else(|| ParseError::new("invalid struct name"))?;

        match iter.next() {
            Some(";") => todo!(), // declaration with no definition
            Some("{") => {
                let mut fields = Vec::new();
                while let Some(&next_token) = iter.peek() {
                    if next_token == "}" {
                        iter.next(); // consume closing brace
                        break;
                    } else {
                        let (ctype, maybe_ident) = crate::ctype::parser::parse_with_identifier_and_subscripts(iter)?;
                        let ident = maybe_ident.ok_or(ParseError::new("could not parse struct field name"))?;
                        fields.push((ident, ctype));
                        let semicolon = iter.next();
                        if semicolon != Some(";") {
                            return Err(ParseError::new(format!("expected semicolon to terminate struct field, found {:?}", semicolon)));
                        }
                    }
                }

                Ok(CStruct::new(struct_name, fields))
            }
            None => Err(ParseError::new("end of file encountered in the middle of struct parse")),
            _ => Err(ParseError::new("invalid token encountered while parsing struct")),
        }
    }
}

